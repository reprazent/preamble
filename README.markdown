`preamble |priːˈamb(ə)l, ˈpriː-| - a preliminary or preparatory statement`

This was started by [Piet Jaspers](https://github.com/pjaspers/preamble), then it was [adapted](https://github.com/10to1/preamble) to setup our Mac Mini as CI and developers macs. And now I'm using it to quickly get a new mac set up.

### What will it do

- Install [Homebrew](http://brew.sh)
- Install common command line tools
- Install several rubies (see `ruby.yml`)
- Install a bunch of applications I use applications
- Remap caps lock to control

#### What should it still do

- Fetch and symlink my dotfiles
- install GDK's

And more, see the `playbook.yml` for more info.

### Steps

- Install Xcode
- `git clone https://gitlab.com/reprazent/preamble.git`
- `sudo easy_install pip; sudo pip install ansible`
- `cd preamble`
- `ansible-playbook playbook.yml --ask-sudo-pass`

Once this is done, installing new things is as easy as changing the `playbook.yml` and rerunning `ansible-playbook playbook.yml --ask-sudo-pass`.

I've also tried to add some tags, so you can target specific parts of it (`ansible-playbook --tags=tag1,tag2`) or skip specific parts (`ansbible-playbook --skip-tags=tag1,tag2`).

Available tags:

- ruby (Install rubies)
- osx (Installs os x applications, and also `capslock` and `defaults`)
- capslock (Remaps capslock to control)
- defaults (Sets Finder defaults)
